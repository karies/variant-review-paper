<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <meta name="author" content="D0086R0, ISO/IEC JTC1 SC22 WG21">
  <title>Variant design review.</title>
  <style type="text/css">code{white-space: pre;}</style>
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link rel="stylesheet" href="markdown3.css">
</head>
<body>
<header>
<h1 class="title">Variant design review.</h1>
<h2 class="author">D0086R0, ISO/IEC JTC1 SC22 WG21</h2>
<h3 class="date">Axel Naumann (axel@cern.ch), 2015-09-24</h3>
</header>
<nav id="TOC">
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#context">Context</a><ul>
<li><a href="#a-variant-is-not-stdany">A <code>variant</code> is not <code>std::any</code></a></li>
<li><a href="#union-versus-variant"><code>union</code> versus <code>variant</code></a></li>
<li><a href="#other-implementations">Other implementations</a></li>
<li><a href="#recursive-variant">Recursive <code>variant</code></a></li>
<li><a href="#visitor">Visitor</a><ul>
<li><a href="#motivation">Motivation</a></li>
<li><a href="#example">Example</a></li>
<li><a href="#return-type-of-visit">Return type of <code>visit()</code></a></li>
<li><a href="#visitor-state">Visitor state</a></li>
<li><a href="#possible-implementation-characteristics">Possible implementation characteristics</a></li>
</ul></li>
<li><a href="#design-considerations">Design considerations</a><ul>
<li><a href="#goals">Goals</a></li>
<li><a href="#a-variant-can-be-invalid">A <code>variant</code> can be invalid</a></li>
<li><a href="#alternatives-duplicate-missing-cv-qualified-and-references">Alternatives: duplicate, missing, cv-qualified and references</a></li>
<li><a href="#constexpr-access"><code>constexpr</code> access</a></li>
<li><a href="#noexcept-interfaces"><code>noexcept</code> interfaces</a></li>
<li><a href="#perfect-initialization">Perfect Initialization</a></li>
<li><a href="#heterogenous-and-element-assignment-conversion-and-relational-operators">Heterogenous and Element Assignment, Conversion and Relational Operators</a></li>
<li><a href="#assignment-emplace">Assignment, Emplace</a></li>
<li><a href="#access-to-address-of-storage">Access to Address of Storage</a></li>
</ul></li>
</ul></li>
<li><a href="#comparing-the-two-proposals">Comparing The Two Proposals</a><ul>
<li><a href="#visibility-of-invalid-state">Visibility of invalid state</a></li>
<li><a href="#undefined-behavior-on-value-extraction-security">Undefined behavior on value extraction != security</a></li>
<li><a href="#default-construction">Default construction</a></li>
<li><a href="#visitation">Visitation</a></li>
<li><a href="#composability">Composability</a></li>
<li><a href="#modeling-the-math">Modeling the math</a></li>
<li><a href="#performance">Performance</a></li>
</ul></li>
<li><a href="#conclusion">Conclusion</a></li>
<li><a href="#acknowledgments">Acknowledgments</a></li>
<li><a href="#references">References</a></li>
</ul>
</nav>
<blockquote>
<p><em>Variant is the very spice of life,</em><br />
 <em>That gives it all its flavor.</em><br />
 - William Cowper’s “The Task”, or actually a variant thereof</p>
</blockquote>
<h1 id="introduction">Introduction</h1>
<p>C++ needs a type-safe union. Given the wide variety of implementations and expectations, and given that two proposals by the same author are available, a combined review of the design choices seemed helpful. This paper contains background information on the proposals P0087 “Variant: a type-safe union without undefined behavior (v2)”, and P0088 “Variant: a type-safe union that is rarely invalid (v5)”, two different incarnations of a <code>variant</code>.</p>
<h1 id="context">Context</h1>
<h2 id="a-variant-is-not-stdany">A <code>variant</code> is not <code>std::any</code></h2>
<p>A <code>variant</code> stores one value out of multiple possible types (the template parameters to <code>variant</code>). It can be seen as a restriction of <code>any</code>. Given that the types are known at compile time, <code>variant</code> allows the storage of the value to be contained inside the <code>variant</code> object.</p>
<h2 id="union-versus-variant"><code>union</code> versus <code>variant</code></h2>
<p>The proposals do not want to replace <code>union</code>: its undefined behavior when casting <code>Apples</code> to <code>Oranges</code> is an often used feature that distinguishes it from <code>variant</code>’s features. So be it.</p>
<p>On the other hand, <code>variant</code> is able to store values with non-trivial constructors and destructors. Part of its visible state is the type of the value it holds at a given moment; it enforces value access happening only to that type.</p>
<h2 id="other-implementations">Other implementations</h2>
<p>The C++ <code>union</code> is a non-type-safe version of <code>variant</code>. <code>boost::variant</code> <span class="citation" data-cites="BOOSTVAR">(1)</span> and <code>eggs::variant</code> <span class="citation" data-cites="EGGSVAR">(2)</span> are similar to this proposal. Their features are discussed in the relevant sections on design discussion.</p>
<h2 id="recursive-variant">Recursive <code>variant</code></h2>
<p>Recursive variants are variants that (conceptually) have itself as one of the alternatives. There are good reasons to add support for a recursive <code>variant</code>; for instance to build AST nodes. There are also good reasons not to do so, and to instead use <code>unique_ptr&lt;variant&lt;...&gt;&gt;</code> as an alternative. A recursive <code>variant</code> can be implemented as an extension to <code>variant</code>, see for instance what is done for <code>boost::variant</code>. The proposals does not contain support for recursive <code>variant</code>s; they also do not preclude a proposal for them.</p>
<p>What <em>is</em> supported by the proposals is a <code>variant</code> that has as one alternative a <code>variant</code> of a different type.</p>
<h2 id="visitor">Visitor</h2>
<h3 id="motivation">Motivation</h3>
<p>A good <code>variant</code> needs a visitor, multimethods, or any other dedicated access method. The proposals include a visitor - not because it’s the optimal design for accessing the elements, but because it <em>is</em> a design. Visitors are common, well understood and thus warrant inclusion in the proposal, independently of future, improved patterns.</p>
<h3 id="example">Example</h3>
<p>The content of a <code>variant</code> can thus be accessed as follows:</p>
<pre><code>variant&lt; ... &gt; var = ...;
visit([](auto&amp; val) { cout &lt;&lt; val; }, var);</code></pre>
<p>using Lambda syntax; or</p>
<pre><code>struct my_visitor {
  template &lt;class AltType&gt;
  ostream&amp; operator()(AltType&amp; var) { cout &lt;&lt; var; return cout; }

  ostream&amp; operator()(const string&amp; s) {
    cout &lt;&lt; &#39;&quot;&#39; &lt;&lt; s &lt;&lt; &#39;&quot;&#39;; return cout;
  }
};

variant&lt;int, int, string&gt; var{&quot;abc&quot;};
visit(my_visitor(), var);</code></pre>
<p>Multi-visitation passes the current value of multiple variants to the visitor function. Example:</p>
<pre><code>struct my_visitor {
  void operator()(...) { cout &lt;&lt; &quot;no match&quot;; }
  void operator()(int i, double d, char c) {
    cout &lt;&lt; i &lt;&lt; &#39; &#39; &lt;&lt; d &lt;&lt; &#39; &#39; &lt;&lt; c;
  }
};

variant&lt;int, string&gt; var1{12};
variant&lt;long, double&gt; var2{13};
variant&lt;string, char&gt; var3{&#39;x&#39;};
visit(my_visitor(), var1, var2, var3); // &quot;12 13.0 x&quot;

var2 = 42;
visit(my_visitor(), var1, var2, var3); // &quot;no match&quot;</code></pre>
<h3 id="return-type-of-visit">Return type of <code>visit()</code></h3>
<p>All <code>callable(T_i)</code> must return the same type to prevent possibly unexpected casts. Future versions can loosen this restriction if it is deemed too constraining.</p>
<h3 id="visitor-state">Visitor state</h3>
<p>Overloads of the <code>visit</code> functions take non-<code>const</code> visitor callables, they allow functions to be invoked that change the state of the callable. Non-mutable lambdas on the other hand require overloads taking <code>const</code> callables.</p>
<h3 id="possible-implementation-characteristics">Possible implementation characteristics</h3>
<p>The closed set of types makes it possible to construct a constexpr array of functions (jump table) to call for each alternative. The visitation of a non-empty <code>variant</code> is then calling the array element at position <code>index()</code>, which is an <code>O(1)</code> operation.</p>
<h2 id="design-considerations">Design considerations</h2>
<h3 id="goals">Goals</h3>
<p>This paragraph has obvious content, yet these goals might get out of sight in the following discussion. Jeffrey Yaskin has conducted a little unauthoritative survey with members of the committee (LEWG, LWG, EWG); the goals mentioned below are trying to summarize the outcome of that and the reflector discussions. Several goals are contradicting.</p>
<ul>
<li>Simplicity: a <code>variant</code> should be simple to understand for simple use cases. Its behavior shall not be surprising.</li>
<li>Performance: if a <code>variant</code> has extra runtime cost, be it CPU cycles or memory, <code>union</code> will remain the type of choice for many. Keeping a <code>variant</code> to the size of the largest alternative size plus a tag, and keeping the operations to the fastest possible (for instance no heap allocations) is a requirement for many to accept the <code>variant</code>.</li>
<li>Generality: a <code>variant</code> shall allow all types as alternatives.</li>
<li>Regularity: a <code>variant</code> shall be as regular as its elements. Notably, it should be default constructible.</li>
<li>Safety: a <code>variant</code> is a type-safe union. That should not come at the price of a built-in security hole in C++, with novice programmers triggering undefined behavior. The ideal <code>variant</code> is robust.</li>
</ul>
<h3 id="a-variant-can-be-invalid">A <code>variant</code> can be invalid</h3>
<p>To simplify the <code>variant</code> and make it conceptually composable for instance with <code>optional</code>, it is desirable that it always contains a value of one of its template type parameters. But the proposed <code>variant</code> designs do have an additional invalid state. Here is why.</p>
<h4 id="the-problem">The problem</h4>
<p>Here is an example of a state transition due to an assignment of a <code>variant</code> <code>w</code> to <code>v</code> of the same type:</p>
<pre><code>variant&lt;S, T&gt; v = S();
variant&lt;S, T&gt; w = T();
v = w;</code></pre>
<p>In the last line, <code>v</code> will first destruct its current value of type <code>S</code>, then initialize the new value from the value of type <code>T</code> that is held in <code>w</code>. If the latter part fails (for instance throwing an exception), <code>v</code> will not contain any valid value. It must not destruct the contained value as part of <code>~variant</code>, and it must make this state visible, because any call of <code>get&lt;T&gt;(v)</code> would have no object to access.</p>
<p>The type changing operation will need to construct an object in the internal buffer of the <code>variant</code>, likely using placement new, and pass the assigned object as an argument to the copy or move constructor. Thus, for the topic of invalid variants, only the behavior of copy and move constructors need to be taken into account.</p>
<h4 id="how-does-union-do-it">How does <code>union</code> do it?</h4>
<p>C++ <code>union</code>s get around this by not managing type transitions. Assignments involving type transitions are too desirable to forbid them for <code>variant</code>. It’s a crucial and fundamental feature.</p>
<h4 id="moving-in-copy-assignment">Moving in Copy Assignment</h4>
<p>A <code>variant</code> can only become invalid in exceptional cases: during a type-changing assignment or emplacement, the copy or move constructor must throw. Generally, a copy constructor is more likely to throw than a move constructor. In the proposed designs, copying a type-changing alternative object into a variant will thus behave as if the object was copied into a temporary, and only upon success of that copy will the variant’s old type be destructed and the temporary moved into the variant. This mechanism is only needed for type-changing assignments for which the copy constructor might throw. This further limits the cases for which invalidity needs to be handled.</p>
<p>In the subsequent discussion we can thus focus on throwing move constructors.</p>
<h4 id="banning-assignment">Banning assignment</h4>
<p>A <code>variant</code> could not offer assignment, neither of an alternative object (see for instance the <code>variant</code> presented at CPPCon 2014 <span class="citation" data-cites="CPPCONVARIANT">(3)</span>) nor of the variant object itself. Without, a <code>variant</code> obviously cannot undergo a type-changing assignment.</p>
<p>This is a huge restriction on the functionality.</p>
<h4 id="requiring-is_nothrow_move_constructible">Requiring <code>is_nothrow_move_constructible</code></h4>
<p>This problem exists only for a subset of types. The exact set of types varies between implementations; implementations are free to add <code>noexcept</code>. Determining whether a type is <code>is_nothrow_move_constructible</code> is not trivial, especially for novice users, especially for types with multiple levels of inheritance and many data members, where all of these need to be checked. Change any member type to an <code>atomic</code>, or add a mutex, and the class cannot be stored in a variant anymore.</p>
<p>Even worse: a large fraction of C++98 “legacy” types have a copy but no move constructor. Putting any of those into a <code>pair</code>, <code>tuple</code>, etc renders the <code>pair</code>, <code>tuple</code>, etc not <code>is_nothrow_move_constructible</code>. See Ville Voutilainen’s paper “We cannot (realistically) get rid of throwing moves”, expected in the pre-Kona mailing.</p>
<p>A <code>variant</code> is a really useful type. Limiting it to <code>is_nothrow_move_constructible</code> types reduces it to a few “good” alternative types; <code>variant</code> will become a powerful, simple tool for specialized use cases, instead of a powerful, mostly simple tool for general use.</p>
<h4 id="requiring-the-first-alternative-to-be-is_nothrow_move_constructible">Requiring the first alternative to be <code>is_nothrow_move_constructible</code></h4>
<p>Peter Dimov suggested [http://lists.isocpp.org/lib-ext/2015/06/0132.php] that a <code>variant&lt;T1,...&gt;</code> that has undergone a throwing type-changing assignment will be set to a default-constructed <code>T1</code> alternative. This requires that <code>T1</code> is no throw default constructible, else assignment of an alternative that is not <code>is_nothrow_copy_constructible</code> will be ill-formed.</p>
<p>This conflates the invalid state with one of the valid states. Peter suggests to introduce an <code>empty</code> type as <code>T1</code> if the variant shall signal its error state.</p>
<p>This does not solve the problem for cases where the variant becomes invalid in one part of the program, and its value is tested in a completely different part of the program. Passing a <code>variant&lt;int, vector&lt;int&gt;&gt;</code> to a library, and not knowing whether the <code>int</code> can be trusted is outright dangerous; it’s a poster child of a security issue. The argument that <code>empty</code> should have been inserted then is not convincing: on my platform, <code>vector&lt;int&gt;</code> is nothrow move constructible. Too bad it isn’t on yours. To write platform-independent safe code, we would need to teach developers to always add <code>empty</code> as first alternative.</p>
<h4 id="changing-no-throw-of-move-constructor">Changing <code>no throw</code> of move constructor</h4>
<p>Some of constructors can throw, for instance standard library types implemented with sentinel nodes. One could however implement traits signaling these types as “once moved, the moved-from object cannot be assigned to”, also known as (semi-) destructive move. This would cover all known implementations of all standard library types; variants could contain all of them as alternatives.</p>
<p>But this still makes these types unusable for instance as data members of user structs. The member type itself could be contained in a variant, because its trait specialization tell variant that it can be moved noexcept, even if maybe only destructively moved. But the compound user type will still not be noexcept move constructible; users would need to specialize the (semi-) destructive move trait for their type. This is far too complex.</p>
<h4 id="double-buffering">Double-buffering</h4>
<p>An alternative used by <code>boost::variant</code> is to introduce a second buffer: <code>v</code> constructs the assigned value in this second buffer, leaving the previous value untouched. Once the construction of the new value was successful, the old value will be destructed and the <code>variant</code> flips type state and remembers that the current value is now stored in the secondary buffer. The disadvantages of a secondary buffer are</p>
<ul>
<li>the address of the <code>variant</code>’s internal storage changes.</li>
<li>additional, up to doubled memory usage: at least the largest alternative type for which <code>is_nothrow_copy_constructible</code> evaluates to <code>false</code> must fit in the secondary buffer (plus a boolean indicating which buffer is currently holding the object);</li>
<li>the additional memory is - at least for <code>boost::variant</code> - allocated when needed in free store; it could also be stored wherever the <code>variant</code>’s storage is; either way there is a possible runtime cost that is hard to predict for users, or an additional size of the <code>variant</code>, rendering it sub-optimal in several cases.</li>
</ul>
<p>An implementation provided by Anthony Williams <span class="citation" data-cites="WILLIAMSVARIANT">(4)</span> tries to use non-throwing move and copy wherever possible, and only reverts to double buffering if at least two alternatives have no non-throwing move construction. This still leaves many <code>variant</code>s with double buffers.</p>
<p>We prefer other options that have a simpler performance pattern. (Up to) doubling the size of a <code>variant</code> in cases that hard difficult to predict for regular users would be a reason for many to continue to use <code>union</code> - after all it is exactly the size optimization for multiple alternatives that makes <code>variant</code> an interesting vocabulary type.</p>
<h4 id="valid-but-only-partially-specified">Valid but only partially specified</h4>
<p>In the proposals, the contained object in an invalid <code>variant</code> can be thought of as a moved-from object: it leaves the <code>variant</code> in a perfectly valid but only partially specified state.</p>
<p>This state needs to be visible: accessing its contents or visiting it will violate preconditions or throw (depending on the proposal); users must be able to verify that a <code>variant</code> is not in this state. Handling the exception from the assignment that creates an empty <code>variant</code> is not always possible; a <code>variant</code> passed to a function might become invalid, cannot be “healed” (all constructors / emplace etc of any alternative type might throw), and so the callee has no way of communicating to the outside that the variant just became invalid.</p>
<p>Instead, we prefer to make this state visible through the <code>index()</code> returning <code>tuple_not_found</code> and a usability feature <code>valid()</code>.</p>
<h4 id="the-partially-in-partially-specified">The “partially” in “partially specified”</h4>
<p>A variant that has undergone a throwing type-changing assignment will contain an object with unspecified state. But the <code>variant</code> knows about its state: <code>valid()</code> has to return <code>false</code>. Copying or moving such a variant has to be allowed (without undefined behavior), to enable scenarios where such a <code>variant</code> is part of a <code>vector</code>, and the <code>vector</code> is resizing. Implementing this is not problematic, due to the visible state of the invalid <code>variant</code>.</p>
<p>Thus the an invalid <code>variant</code> contains an object of unspecified state while it itself has a well specified state (invalid).</p>
<h4 id="no-strong-exception-guarantee">No strong exception guarantee</h4>
<p>The variant cannot provide a strong exception guarantee if any of its alternatives can throw in move construction, unless double buffering is employed.</p>
<h4 id="empty-state-and-default-construction">Empty state and default construction</h4>
<p>Default construction of a <code>variant</code> should be allowed, to increase usability for instance in containers. Options are: - default construct by default constructing the first alternative if that is default constructible - add a dedicated type as first alternative that explicitly enables default construction and adds an explicit new state for the default constructed <code>variant</code> - default construct into the invalid (or empty) state</p>
<p>A union default constructs the first member, similar to the first option here.</p>
<h3 id="alternatives-duplicate-missing-cv-qualified-and-references">Alternatives: duplicate, missing, cv-qualified and references</h3>
<h4 id="variantint-int"><code>variant&lt;int, int&gt;</code></h4>
<p>Multiple occurrences of identical types are allowed. They are distinct states; the <code>variant</code> can contain either the first or the second <code>int</code>. This models a discriminated union. For a <code>variant</code> with duplicate types in the alternatives, use of the interfaces that identify the alternative through a type template parameter (constructor, emplace, get, etc) is ill-formed. Instead, <code>get&lt;0&gt;</code> has to be used in place of <code>get&lt;int&gt;</code>; emplacement-with-index instead of assignment or type-based emplacement; and construction providing an emplacement hint (<code>emplaced_index&lt;0&gt;</code>) instead of a construction passing an <code>int</code>.</p>
<h4 id="void-as-an-alternative"><code>void</code> as an alternative</h4>
<p>Again to facilitate meta-programming, <code>void</code> is an acceptable template type parameter for a <code>variant</code>. The <code>variant</code> will never store an object of this type, the position of the <code>void</code> alternative will never be returned by <code>index()</code>.</p>
<h4 id="variant"><code>variant&lt;&gt;</code></h4>
<p>A <code>variant</code> without alternatives cannot be constructed; it is otherwise an allowed type. It is easier to allow it than to forbid it.</p>
<h4 id="variantint-const-int"><code>variant&lt;int, const int&gt;</code></h4>
<p>A <code>variant</code> can handle <code>const</code> types: they can only be set through <code>variant</code> construction and <code>emplace()</code>. If both <code>const</code> and non-<code>const</code> types are alternatives, the active alternative is chosen by regular constructor instantiation / overload rules, just as for any other possibly matching alternative types.</p>
<h4 id="variantint"><code>variant&lt;int&amp;&gt;</code></h4>
<p>References are supported as alternative types. Assignment to such a value is ill-formed.</p>
<h3 id="constexpr-access"><code>constexpr</code> access</h3>
<p>Many functions of <code>variant</code> can be marked <code>constexpr</code> without requiring “compiler magic” due to <code>reinterpret_cast</code>s of the internal buffer. This is strictly an extension of how <code>constexpr</code> can be implemented for the interfaces of <code>optional</code>; possible implementations involve recursive unions.</p>
<h3 id="noexcept-interfaces"><code>noexcept</code> interfaces</h3>
<p>The <code>variant</code> should ideally have the same <code>noexcept</code> clauses as <code>tuple</code>.</p>
<h3 id="perfect-initialization">Perfect Initialization</h3>
<p>The proposals employ the same mechanisms for perfect initialization <span class="citation" data-cites="PERFECTINIT">(5)</span> as <code>optional</code>; see the discussion there. A constructor tag <code>emplaced_type</code> is used to signal the perfect forwarding constructor overload. For symmetry reasons, <code>emplace_index&lt;I&gt;</code> can be used to signal the initialization to the I-th alternative.</p>
<h3 id="heterogenous-and-element-assignment-conversion-and-relational-operators">Heterogenous and Element Assignment, Conversion and Relational Operators</h3>
<p>The proposals follows the implementation of Boost.Variant and Eggs.Variant and only provides same-type relational operators. This is partially a consequence of the LEWG review, partially a requirement of <code>variant</code> being a regular type. As an example, transitivity can be violated if variants can be compared with their values:</p>
<pre><code>variant_with_element_less&lt;float, int&gt; vi(12), vf(14.);
assert(
  vi &lt; 13. &amp;&amp; 13. &lt; vf &amp;&amp; vf &lt; vi &amp;&amp;
  R&quot;quote(
    &quot;Oh dear,&quot; says Arian, &quot;I hadn&#39;t thought of that,&quot;
    and promptly vanishes in a puff of logic.
)quote&quot;)</code></pre>
<p>A possible later extension that seems to still keep <code>variant</code> as a regular type are comparison operators of the form and behavior</p>
<pre><code>template &lt;class T, class... Types&gt;
bool operator==(const variant&lt;Types...&gt;&amp; v, const T&amp; t) {
    return v == variant&lt;Types...&gt;{t}; // exposition only
}</code></pre>
<p>for all alternative types <code>T</code>. They can be implemented in a more performant way than what is suggested here. There have been voices questioning that this variant would still be regular, though no proof has been found to this date.</p>
<h3 id="assignment-emplace">Assignment, Emplace</h3>
<p>The assignment and emplace operations of <code>variant</code> model that of <code>optional</code>; also <code>variant</code> employs same-type optimizations, using the assignment operator instead of construction if the <code>variant</code> already contains a value of the assigned / emplaced type.</p>
<h3 id="access-to-address-of-storage">Access to Address of Storage</h3>
<p>Given that <code>variant</code> is type safe, access to the address of its internal storage is not provided. If really needed, that address can be determined by using a visitor.</p>
<h1 id="comparing-the-two-proposals">Comparing The Two Proposals</h1>
<h2 id="visibility-of-invalid-state">Visibility of invalid state</h2>
<p>The major difference of the two proposals is how much the invalid state is exposed. The <code>variant</code> “without undefined behavior” argues that this state exists and has to be dealt with; no rug is large enough to change that. It naturally plays the role of an additional state of the <code>variant</code> with N alternatives, turning a type with N states into one with N+1 states. It makes it even more visible by providing a <code>clear()</code> function.</p>
<p>The “rarely invalid” <code>variant</code> argues that this state should really never be reached. The <code>variant</code> will try to be helpful for this degenerate state, but not at the cost of cluttering user code by forcing an extra validity check to element accesses because the invalid state is too common.</p>
<h2 id="undefined-behavior-on-value-extraction-security">Undefined behavior on value extraction != security</h2>
<p>Even though the “rarely invalid” <code>variant</code> makes an invalid <code>variant</code> very unlikely, it can still exist. Developers cannot rely on a given <code>variant</code> to have nothrow move constructible alternatives: this might change over implementations and / or time. Now they have to options: given how rare an invalid <code>variant</code> is, they just ignore that state in their program. If it ever happens (for instance triggered by an evil person) then element accesses will trigger undefined behavior, which can cause invalid data read, which can in turn have security implications.</p>
<p>The alternative is to always handle the very unlikely invalid state. That feels as futile as checking the return value of <code>printf()</code> if it basically never happens; it will be hard to convince developers that it’s nonetheless crucial. Suppose we would convince all developers to always check for validity: then we are back to the exposed state of the <code>variant</code> “without undefined behavior”, at the additional cost of introducing undefined behavior in some conditions. And by the way, undefined behavior in one of the fundamental operations of one of the fundamental types of a language is clearly <em>not</em> desirable.</p>
<h2 id="default-construction">Default construction</h2>
<p>So we have a visible invalid state. Default constructing to that state is a simple and obvious solution. This is similar to <code>optional</code>, <code>ifstream</code>, <code>unique_ptr</code> - at least in spirit.</p>
<p>Default constructing to invalid makes sure that developers realize that a <code>variant</code> can be invalid.</p>
<h2 id="visitation">Visitation</h2>
<p>The “rarely invalid” <code>variant</code> will rely on the caller of the visitation to ensure that no invalid <code>variant</code> is passed; else the behavior is undefined.</p>
<p>For the <code>variant</code> “without undefined behavior”, visitation will throw if an invalid <code>variant</code> is passed. The original proposal (N4218) suggested to call an visitor overload that takes no argument for an invalid <code>variant</code>. This is not compatible with multi-visitation and has thus been revised.</p>
<h2 id="composability">Composability</h2>
<p>Focused types are good. It is desirable to separate the ideas of <code>optional</code> and <code>variant</code>; <code>optional&lt;variant&lt;int, float&gt;&gt;</code> is more explicit than just <code>variant&lt;int, float&gt;</code>. This suggests the “rarely invalid” <code>variant</code> is the superior type.</p>
<h2 id="modeling-the-math">Modeling the math</h2>
<p>Type theory prefers the “rarely invalid” <code>variant</code> (and even more so the double-buffering one) because it does not add a new state. Developers coming from other languages will have a smoother transition to C++, at least it might be perceived as such.</p>
<h2 id="performance">Performance</h2>
<p>The accesses to a <code>variant</code> need to check whether the currently active variant alternative corresponds to the alternative requested in the access, i.e. whether for <code>get&lt;0&gt;(v)</code>, the active alternative of <code>variant v</code> is the first one. An additional state introduces no relevant performance penalty. All interfaces exhibiting undefined behavior on the “rarely invalid” <code>variant</code> are designed to throw an exception already; here, too, the proposal “without undefined behavior” does not add a runtime cost.</p>
<p>Both designs need to equally capture throwing move construction of an alternative; again, there not introducing any conceivable performance penalty on either side.</p>
<h1 id="conclusion">Conclusion</h1>
<p>A <code>variant</code> has proven to be a useful tool. This paper shows the complexity behind it.</p>
<h1 id="acknowledgments">Acknowledgments</h1>
<p>Thank you, Nevin “:-)” Liber, for bringing sanity to this proposal. Agustín K-ballo Bergé and Antony Polukhin provided very valuable feedback, criticism and suggestions. Thanks also to Vincenzo Innocente and Philippe Canal for their comments.</p>
<h1 id="references" class="references unnumbered">References</h1>
<div id="ref-BOOSTVAR">
<p>1. <em>Boost.Variant</em> [online]. Available from: <a href="http://www.boost.org/doc/libs/1_56_0/doc/html/variant.html" class="uri">http://www.boost.org/doc/libs/1_56_0/doc/html/variant.html</a></p>
</div>
<div id="ref-EGGSVAR">
<p>2. <em>Eggs.Variant</em> [online]. Available from: <a href="http://eggs-cpp.github.io/variant/" class="uri">http://eggs-cpp.github.io/variant/</a></p>
</div>
<div id="ref-CPPCONVARIANT">
<p>3. <em>Variant by Jason Lucas at CppCon</em> [online]. Available from: <a href="https://github.com/JasonL9000/cppcon14" class="uri">https://github.com/JasonL9000/cppcon14</a></p>
</div>
<div id="ref-WILLIAMSVARIANT">
<p>4. <em>Variant by Anthony Williams</em> [online]. Available from: <a href="https://bitbucket.org/anthonyw/variant" class="uri">https://bitbucket.org/anthonyw/variant</a></p>
</div>
<div id="ref-PERFECTINIT">
<p>5. <em>Improving pair and tuple, revision 2</em>. N4064</p>
</div>
</body>
</html>
