NAME := variant-review
DOCNUM := P0086R0
IN   := $(NAME).md bib.bib
OUT  := $(DOCNUM).html $(DOCNUM).pdf
CSS  := markdown3.css
PANDOCFLAGS := --css=$(CSS) -s -S --toc --from=markdown --bibliography=bib.bib --csl=iso690-numeric-en.csl $(NAME).md

all: $(OUT)

$(DOCNUM).html: $(IN) $(CSS)
	pandoc $(PANDOCFLAGS) --to=html5 -o $@

$(DOCNUM).pdf: $(IN) $(CSS)
	pandoc $(PANDOCFLAGS) --template=template.tex --variable documentnumber=$(DOCNUM) -o $@
clean:
	rm -f $(OUT)
